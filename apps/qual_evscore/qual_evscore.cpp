/***************************************************************************
 *   Copyright (C) by ETHZ/SED                                             *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU Affero General Public License as published*
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Affero General Public License for more details.                     *
 *                                                                         *
 *                                                                         *
 * Developed by Stefan Heimers and Tobias Diehl, Swiss Seismological       *
 * Service Based on an example by Jan Becker of GEMPA                      *
 ***************************************************************************/

/*
 * This code is uded by scolv to retrieve the evscore and other
 * quality information from an origin and display it.
 */

#include <seiscomp3/datamodel/comment.h>
#include <seiscomp3/datamodel/origin.h>
#include <seiscomp3/io/archive/binarchive.h>
#include <iostream>
#include <sstream>
#include <string>

#include "get_score.h"

using namespace std;
using namespace Seiscomp;

inline bool endsWith(std::string const &  haystack, std::string const &  needle){
  if (needle.size() > haystack.size()) return false;
  return std::equal(needle.rbegin(), needle.rend(), haystack.rbegin());
}

int main(int argc, char **argv) {
  std::string qType = "";
  if (argc > 1) { qType = argv[1]; }
  string q_evscore = "-";
  IO::BinaryArchive ar;
  if (ar.open("-")) {
    DataModel::OriginPtr org;
    ar >> org;
    if (org && qType == "") {
      for (size_t i = 0; i < org->commentCount(); i++) {
        DataModel::Comment *cmt = org->comment(i);
        if (cmt->id() == "smi:ch.ethz.sed/originquality/quality.evscore") {
          // get the event score text from the stored score in the commend
          q_evscore = cmt->text();
          break;
        }
      }
      /* This computes the score in case a comment is not present
      if ( q_evscore == "-" )
      {
          double actualScore = get_score(org.get());
          ostringstream sstream;
          sstream << actualScore;
          q_evscore = "(" + sstream.str() + ")";
      }
      */
    }
    else if (org && qType == "-nll") {
      std::string method = "";
      std::string q_nll  = "none"; 
      for (size_t i = 0; i < org->commentCount(); i++) {
        DataModel::Comment *cmt = org->comment(i);
        if (endsWith(cmt->id(),"quality.nll.L2")) {
	  q_nll = cmt->text();
          method = "(L2)";
          break;
        }
        if (endsWith(cmt->id(),"quality.nll.EDT")) {
	  q_nll = cmt->text();
          method = "(EDT)";
          break;
        }
        if (endsWith(cmt->id(),"quality.nll")) {
	  q_nll = cmt->text();
          method = "(?)";
          break;
        }
      }
      if (q_nll == "1") q_evscore = "D";
      else if (q_nll == "2") q_evscore = "C";
      else if (q_nll == "3") q_evscore = "B";
      else if (q_nll == "4") q_evscore = "A";                  
      else q_evscore = "-";
      q_evscore += method;
    }
    else if (org && qType == "-res") {
      std::string q_nll  = "none"; 
      for (size_t i = 0; i < org->commentCount(); i++) {
        DataModel::Comment *cmt = org->comment(i);
        if (endsWith(cmt->id(),"quality.res")) {
	  q_nll = cmt->text();
          break;
        }
      }
      if (q_nll == "1") q_evscore = "D";
      else if (q_nll == "2") q_evscore = "C";
      else if (q_nll == "3") q_evscore = "B";
      else if (q_nll == "4") q_evscore = "A";                  
      else q_evscore = "-";
    }
    
  }
  cout.setf(std::ios::unitbuf);
  cout << q_evscore;
  return EXIT_SUCCESS;
}
