#!/usr/bin/env seiscomp-python

###########################################################################
#   Copyright (C) by ETHZ/SED                                             #
#                                                                         #
# This program is free software: you can redistribute it and/or modify    #
# it under the terms of the GNU Affero General Public License as published#
# by the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                     #
#                                                                         #
# This program is distributed in the hope that it will be useful,         #
# but WITHOUT ANY WARRANTY; without even the implied warranty of          #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           #
# GNU Affero General Public License for more details.                     #
#                                                                         #
#                                                                         #
# Developed by Stefan Heimers and Tobias Diehl, Swiss Seismological       #
# Service Based on an example by Jan Becker of GEMPA                      #
###########################################################################/

# This script does subscribe to the sc3 messageing, listen for LOCATION
# messages and calculates sed style [ABCD] quality ratings for origins.
# The origin quality is updated in the comment table of the sc3 database,
# the quality rating from this script will be appended, separated by a /.

#TD: This version splits NLL qualities into L2 and EDT
 
import os, sys, traceback
import seiscomp.client, seiscomp.seismology
import seiscomp.logging, seiscomp.io
import seiscomp.datamodel
import numpy as np
from evscoremodule import eventscore

class LocationListener(seiscomp.client.Application):

    def __init__(self):
        seiscomp.client.Application.__init__(self, len(sys.argv), sys.argv)
        self.setMessagingEnabled(True)
        self.setDatabaseEnabled(True, True)
        self.setPrimaryMessagingGroup(seiscomp.client.Protocol.LISTENER_GROUP)

    def createCommandLineDescription(self):
        self.commandline().addGroup("Mode")
        self.commandline().addStringOption("Mode", "ep", "Event parameters XML file for offline processing of contained origins")

    def updateObject(self, parentID, object):
        # called if an updated object is received
        self.is_origin(object)

    def addObject(self, parentID, object):
        # called if a new object is received
        self.is_origin(object)

    def validateParameters(self):
        if not seiscomp.client.Application.validateParameters(self):
            return False

        self.xmlMode = self.commandline().hasOption("ep")

        if self.xmlMode:
            seiscomp.logging.info("XML processing: disabling messaging and database...")
            self.setMessagingEnabled(False)
            self.setDatabaseEnabled(False, False)
            ar = seiscomp.io.XMLArchive()
            xmlfile = self.commandline().optionString("ep") 
            if ar.open(xmlfile) == False:
                seiscomp.logging.debug("Could not open %s" % xmlfile)
                return False
            obj = ar.readObject()
            ar.close()
            if obj is None:
               seiscomp.logging.debug("Empty document %s" % xmlfile)
               return False
            self.eventParameters = seiscomp.datamodel.EventParameters.Cast(obj)
            if self.eventParameters is None:
               seiscomp.logging.debug("Expected EventParameters, got %s" % obj.className())
               return False

        return True

    def run(self):

        # offline processing of XML file
        if self.xmlMode:
            cnt = self.eventParameters.originCount()
            for i in range(cnt):
                org = self.eventParameters.origin(i)
                self.is_origin(org)
            ar = seiscomp.io.XMLArchive()
            ar.create("-")
            ar.setFormattedOutput(True)
            ar.writeObject(self.eventParameters)
            ar.close() 
            return True

        seiscomp.logging.debug("The LocationListener is now ready.")
        return seiscomp.client.Application.run(self)


# locqual_baer is borrowed from Location_Quality_bkreloc.py by Tobias Diehl
    def locqual_baer(self,res,gap,mdi,rms,tA,tB,tC):
        """Calculates Location Quality implemented in Manfred Baer's system
        Threshold for Quality A residuals (     |res| <= tA : Quality A)
        Threshold for Quality A residuals (tA < |res| <= tB : Quality B)
        Threshold for Quality C residuals (tB < |res| <= tC : Quality C)
        Threshold for Quality D residuals  tC < |res|

        New version also uses gap and minimum-distance and rms as possible criteria
        gap   = Azimuthal gap of event (from origin)
        mdi   = distance to closest station (km) (from origin)
        rms   = RMS of solution (s)

        T. Diehl
        """
        n=len(res)
        #Initial value:
        minp = 6                            #Minimum number of phases
        perc = 75                           #Minimum percentage of picks better than D
        maxg = 270                          #Maximum gap (degree) (if larger, origin is of quality D)
        mmdi = 200.0                        #Maximum distance to closest station (km) (if larger, origin is of quality D) For no restriction use: 180*111.1949
        maxr = 0.5                          #Maximum RMS (s), (if larger, origin is of quality D)
        qalc = ['A','B','C','D']            #Definition of quality classes
        nolc = len(qalc)                    #Number of quality classes
        qalv = np.zeros(nolc,dtype=int)  #Vector with hit-counts for each class


        #Check cut-off values right at the beginning:
        #--------------------------------------------
        #Check Gap: if larger than maxg, it is lowest quality 'D'
        if(gap > maxg):
           qual = 'D'
           seiscomp.logging.debug("Azimuthal Gap is larger than maximum allowed gap of %s" % maxg)
           seiscomp.logging.debug("locqual_baer: qual is %s" % qual)
           return qual
          
        #Check RMS: if larger than maxr, it is lowest quality 'D'
        if(rms > maxr):
           qual = 'D'
           seiscomp.logging.debug("RMS is larger than maximum allowed RMS %s" % maxr)
           seiscomp.logging.debug("locqual_baer: qual is %s" % qual)
           return qual 

        #Check distance to closest station: f larger than mmdi, it is lowest quality 'D'
        if(mdi > mmdi):
           qual = 'D'
           seiscomp.logging.debug("Distance to closest station is larger than maximum allowed distance %s" % mmdi)
           seiscomp.logging.debug("locqual_baer: qual is %s" % qual)
           return qual

        #All cut-off values passed, now calculate residual distribution:
        #---------------------------------------------------------------
        #Loop over residuals: 
        for i in range(0,n):
            if(abs(res[i]) <= tA):
                qalv[0] += 1
            if(abs(res[i]) > tA) and (abs(res[i]) <= tB):
                qalv[1] += 1
            if(abs(res[i]) > tB) and (abs(res[i]) <= tC):
                qalv[2] += 1
            if(abs(res[i]) > tC):
                qalv[3] += 1
        seiscomp.logging.debug("locqual_baer: qalv is %s" %  qalv)

        #Now check if arrivals of class A-C exceed minp, otherwise it is lowest quality 'D'
        if(n-qalv[3]<minp):
           qual = 'D'
           seiscomp.logging.debug("Number of arrivals of class A-C is less than %s" % minp)
           seiscomp.logging.debug("locqual_baer: qual is %s" % qual)
           return qual

        #Now check if arrivals of class A-C exceed perp, otherwise it is lowest quality 'D'
        perp = int(round((perc/100.0)*n))
        if(n-qalv[3]<perp):
           qual = 'D'
           seiscomp.logging.debug("Percentage of arrivals of class A-C is less than %s" % perp)
           seiscomp.logging.debug("locqual_baer: qual is %s" % qual)
           return qual

        #Find mode:
        maxi = np.argmax(qalv)
        maxp = max(qalv)

        #This is already an extension introduced by TD for small numbers of picks:
        #The prefered class needs at least "minp" hits, otherwise we merge it with
        #lower classes, until enough phases are collected.

        #NEW: For the total number of picks, we should also consider residuals of higher
        #     quality than the current class. E.g. we have
        #     A: 2
        #     B: 2
        #     C: 3
        #     D: 1 
        #     Would result in quality D, although we have more than 6 picks with quality better than D.
        #     Therefore, we add picks of quality classes better than the current (maxi) to maxp
        #seiscomp.logging.debug("locqual_baer: maxi is %s" %  maxi)
        #seiscomp.logging.debug("locqual_baer: maxp is %s" %  maxp)
        for i in range(0,maxi):
            maxp += qalv[i]
        #seiscomp.logging.debug("locqual_baer: maxp is %s" %  maxp)

        #Check if maxp >= minp; if not use lower class untill enough phases are available
        if(maxp < minp):
            for i in range(maxi+1,nolc):
                maxi = i             #Update index of quality class
                maxp = maxp+qalv[i]  #Add number of phases of current class

                #Check if maxp >= minp after update, if not, next iteration
                if(maxp >= minp):    
                    break            
        qual = qalc[maxi]

        seiscomp.logging.debug("locqual_baer: qual is %s" % qual)
        return qual


    def sedqual_from_origin(self,org):
        try:
            gap=org.quality().azimuthalGap()
        except:
            seiscomp.logging.debug("gap not found for origin, skipping")
            return 'D','None'
        try:
            mdi=org.quality().minimumDistance()
        except:
            seiscomp.logging.debug("mdi not found for origin, skipping")
            return 'D','None'
        try:
            rms=org.quality().standardError()
        except:
            seiscomp.logging.debug("rms not found for origin, skipping")
            return 'D','None'
        try:
            met=org.methodID()
        except:
            seiscomp.logging.debug("met not found for origin, skipping")
            return 'D','None'
        seiscomp.logging.debug("Minimum distance %s" % mdi)
        seiscomp.logging.debug("Azimuthal GAP    %s" % gap)
        seiscomp.logging.debug("RMS              %s" % rms)
        seiscomp.logging.debug("Method           %s" % met)
        seiscomp.logging.debug("Nr. of arrivals  %d" % org.arrivalCount())
        
        residuals = []
        if (org.arrivalCount() > 0):
            for i in range(org.arrivalCount()):
                try:
                    seiscomp.logging.debug("arrival nr. %d" % i)
                    residuals.append(org.arrival(i).timeResidual())
                    seiscomp.logging.debug("residual: %f" %  residuals[i])
                except:
                    seiscomp.logging.debug("Problem determining arrival nr. %d" % i)
            sedqual=self.locqual_baer(residuals,gap,mdi,rms,0.15,0.3,0.6)
            seiscomp.logging.debug("sedqual_from_origin: sedqual is %s" % sedqual)
        else:
            seiscomp.logging.debug("no residuals found, setting default quality of D")
            sedqual='D'
        return sedqual,met

    def __send_message(self):
        if not self.xmlMode:
            msg = seiscomp.datamodel.Notifier.GetMessage()
            self.connection().send(msg)

    def update_q_res_in_db(self,org, sedqual):
        seiscomp.logging.debug("update_q_res_in_db(): started")
        for i in range(org.commentCount()):
            if org.comment(i).id() == "smi:ch.ethz.sed/originquality/quality.res":
                seiscomp.logging.debug("update_q_res_in_db(): ...quality.res already found, removing")
                org.removeComment(i)
                break

        new_comment = seiscomp.datamodel.Comment()
        new_comment.setId("smi:ch.ethz.sed/originquality/quality.res")
        if sedqual == 'A':
            sedqual_num='4'
        if sedqual == 'B':
            sedqual_num='3'
        if sedqual == 'C':
            sedqual_num='2'
        if sedqual == 'D':
            sedqual_num='1'
        new_comment.setText("%s" % sedqual_num)
        org.add(new_comment)
        seiscomp.logging.debug("debug: org.add(new_comment) done, new_comment for ...quality.res is %s" % new_comment)
        self.__send_message()


    def update_q_nll_in_db(self,org,omethod):
        seiscomp.logging.debug("update_q_nll_in_db(): started")
        qual_nll = "D"
        qual_nll_num="-1"

        #Check which is the correct NLL-branch:
        seiscomp.logging.debug("origin_method is: %s" % omethod)
        #EDT case:
        if(omethod == "NonLinLoc(EDT)"):
           clabel = "smi:ch.ethz.sed/originquality/quality.nll.EDT"
        #L2  case:
        else:
           clabel = "smi:ch.ethz.sed/originquality/quality.nll.L2"

        for i in range(org.commentCount()):
            if org.comment(i).id() == "SED.quality":
                # get the SED quality text (C/B style) from the database
                old_q = org.comment(i).text()
                seiscomp.logging.debug("update_q_nll_in_db(): old_q from db is: %s" % old_q )
                qual_nll = old_q.split('/')[0]
                seiscomp.logging.debug("update_q_nll_in_db(): ...qual_nll  is: %s" % qual_nll )
                break

        # remove any already existing field for this quality type from this origin
        for i in range(org.commentCount()):
            if org.comment(i).id() == clabel:
                seiscomp.logging.debug("update_q_nll_in_db(): %s already found, removing" % clabel )
                org.removeComment(i)
                break

        new_comment = seiscomp.datamodel.Comment()        
        new_comment.setId(clabel)
        seiscomp.logging.debug("update_q_nll_in_db(): ...qual_nll is: %s" % qual_nll) 
        if qual_nll == 'A':
            qual_nll_num='4'
        if qual_nll == 'B':
            qual_nll_num='3'
        if qual_nll == 'C':
            qual_nll_num='2'
        if qual_nll == 'D':
            qual_nll_num='1'
        new_comment.setText("%s" % qual_nll_num)
        org.add(new_comment)
        seiscomp.logging.debug("debug: org.add(new_comment) done, new_comment for ...quality.nll is %s" % new_comment)
        self.__send_message()


    def update_score_in_db(self,org,score):
        seiscomp.logging.debug("update_score_in_db started")
        if score == -1E23:
            seiscomp.logging.debug("The score could not be computed, not writing to the database")
            return
        if not self.xmlMode:
            seiscomp.datamodel.Notifier.SetEnabled(True)
        old_score_found=0
        for i in range(org.commentCount()):
            if org.comment(i).id() == "smi:ch.ethz.sed/originquality/quality.evscore":
                org.comment(i).setText("%f" % score)
                org.comment(i).update()
                old_score_found=1
                break
        if old_score_found <= 0:
            seiscomp.logging.debug("debug: no ...quality.evscore comment found, creating new one")
            new_comment = seiscomp.datamodel.Comment()
            new_comment.setId("smi:ch.ethz.sed/originquality/quality.evscore")
            new_comment.setText("%f" % score)
            org.add(new_comment)
            seiscomp.logging.debug("debug: add() done for score")
        self.__send_message()
        seiscomp.logging.debug("debug: send() done for score")


    def update_quality_in_db(self,org,sedqual,omethod):
        if not self.xmlMode:
            seiscomp.datamodel.Notifier.SetEnabled(True)
        old_q_found=0
        for i in range(org.commentCount()):
            if org.comment(i).id() == "smi:ch.ethz.sed/originquality/SED.quality":
                # get the previous SED quality text from the database
                old_q = org.comment(i).text()
                seiscomp.logging.debug("update_quality_in_db: old_q from db is: %s" % old_q )
                old_q_left = old_q.split('/')[0]
                new_q = "%s/%s" % (old_q_left, sedqual)
                seiscomp.logging.debug("debug: new_q is: %s" % new_q)
                # now store the new SED quality in the database    
                # Figure out new comment text
                org.comment(i).setText(new_q)
                org.comment(i).update()
                old_q_found=1
                break
        if old_q_found <= 0:
            seiscomp.logging.debug("debug: no SED.quality comment found, creating new one")
            new_comment = seiscomp.datamodel.Comment()
            new_comment.setId("smi:ch.ethz.sed/originquality/SED.quality")
            new_comment.setText("/%s" % sedqual)
            org.add(new_comment)
            seiscomp.logging.debug("debug: add() done for ")

        self.__send_message()

        # now produce the numerical quality comments
        self.update_q_res_in_db(org, sedqual)
        if omethod in ("NonLinLoc(EDT)", "NonLinLoc(L2)"):
            self.update_q_nll_in_db(org, omethod)
        seiscomp.logging.debug("update_quality_in_db(): finished")


    def is_origin(self,object):
        org = seiscomp.datamodel.Origin.Cast(object)
        if org:
            seiscomp.logging.debug("processing origin %s" % org.publicID())
            q_new,o_meth = self.sedqual_from_origin(org)
            self.update_quality_in_db(org,q_new,o_meth)
            seiscomp.logging.debug("about to calculate score")
            try:
                seiscomp.logging.debug("before eventscore")
                evscore = eventscore(org.publicID())
                seiscomp.logging.debug("after eventscore")
            except:
                seiscomp.logging.debug("event_score() failed, returning without an event score")
                return
            seiscomp.logging.debug("evscore: %f" % evscore)
            seiscomp.logging.debug("about to write score to the database")
            self.update_score_in_db(org,evscore)
            seiscomp.logging.debug("is_origin() finished")
            return org
        return None


# main program starts here
app = LocationListener()
sys.exit(app())

