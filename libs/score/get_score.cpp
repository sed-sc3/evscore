/***************************************************************************
 *   Copyright (C) by ETHZ/SED                                             *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU Affero General Public License as published*
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Affero General Public License for more details.                     *
 *                                                                         *
 *                                                                         *
 * Developed by Stefan Heimers and Tobias Diehl, Swiss Seismological       *
 * Service Based on an example by Jan Becker of GEMPA                      *
 ***************************************************************************/

#define SEISCOMP_COMPONENT EvScore

#include "get_score.h"
#include <seiscomp3/logging/log.h>
using namespace std;


/*!
\brief get_score does the actual calculation of the event score
*/
double get_score(Seiscomp::DataModel::Origin *origin) {
  // Initial score value
  double score = -1E23;

  if (!origin) {
    SEISCOMP_INFO("Null origin");
    return score;
  }

  SEISCOMP_DEBUG("get_score() called with origin public id %s",
                 origin->publicID().c_str());

  /// Set some parameters:
  double fperc = 75;   // Percentile of picks within residual <= rema
  double frema = 0.6;  // Residual threshold in seconds, residuals > rthres
                       // considered outliers
  double fcgap = 310;  // critical gap (gap > cgap considered poor-quality)
  double fegap = 5;    // Exponent for gap-term (l)
  double fcrms = 0.5;  // critical rms (rms > crms considered poor-quality)
  double ferms = 5;    // Exponent for rms-term (m)
  double fcnob = 5;    // Minimum number of picks
  double fenob = 5;    // Exponent for pick-term
  // double fenobt  = 1;       //Exponent for total number of picks
  // double fcmdi   = 100;     //Critical closest-station distance (km)
  // double femdi   = 5;       //Exponent for closest-station distance

  std::string agencyID;
  try {
    agencyID = origin->creationInfo().agencyID();
  } catch (...) { }

  double fgap, frms; //, fmdi;
  try {
    /// extract the azimuthal gap and the standard error from the origin object
    fgap = origin->quality().azimuthalGap();
    //fmdi  = origin->quality().minimumDistance();
    frms = origin->quality().standardError();
  } catch (...) {
    SEISCOMP_INFO("WARNING: No quality information for origin: %s by %s",
                  origin->publicID().c_str(), agencyID.c_str());
    return score;
  }

  // Get the number of arrivals associated with the origin
  // and also extract used residuals
  std::list<double> reslist;

  for (size_t i = 0; i < origin->arrivalCount(); i++) {
    Seiscomp::DataModel::Arrival *arrival = origin->arrival(i);

    // skip arrivals not used in (re)location
    try {
      if (arrival->weight() == 0 || !arrival->timeUsed()) {
        continue;
      }
    } catch (Seiscomp::Core::ValueException &) {
    }

    // fetch residuals
    try {
      reslist.push_back(abs(arrival->timeResidual()));
    } catch (...) {
      SEISCOMP_DEBUG("timeresidual for arrival nr. %d not found, %s, %s",
                     (int)i, origin->publicID().c_str(), agencyID.c_str());
    }
  }

  size_t narr = reslist.size();

  if (narr < 1) {
    SEISCOMP_INFO("WARNING: No arrivals for origin: %s by %s",
                  origin->publicID().c_str(), agencyID.c_str());
    return score;
  }

  // Sort residuals:
  reslist.sort();

  SEISCOMP_DEBUG("Residuals (sorted reslist):");
  list<double>::iterator it;
  for (it = reslist.begin(); it != reslist.end(); it++) {
    SEISCOMP_DEBUG("%f", *it);
  }

  // Total number of residuals used in location:
  size_t nreto = reslist.size();

  // Number of picks with residual <= rthres (non-outliers)
  size_t nreno = 0;

  for (it = reslist.begin(); it != reslist.end(); it++) {
    if (*it <= frema)
      nreno += 1;
    else
      break;
  }

  // Get 75-percentile:
  int nperc = (int)((fperc / 100.0) * nreto);

  // get residual #nperc of
  // qx = reslist[nperc-1];
  for (int i = 0; i < (nperc - 1); i++) {
    reslist.pop_front();
  }
  double qx = reslist.front();

  /// Now calculate score:
  // PK original proposal:
  /// score = (qx + pow((fgap/fcgap),fegap) + pow((frms/fcrms),ferms) +
  /// pow((fcnob/((fperc/100.0)*nreto)),fenob) ) * -1;
  score = (qx + pow((fgap / fcgap), fegap) + pow((frms / fcrms), ferms) +
           pow((fcnob / ((fperc / 100.0) * nreto)), fenob)) *
          -1;

  // Some information to screen:
  SEISCOMP_DEBUG("nreno  %d", (int)nreno);
  SEISCOMP_DEBUG("fcnob  %f", fcnob);
  SEISCOMP_DEBUG("fenob  %f", fenob);
  SEISCOMP_DEBUG("term #4 %d", (int)pow((fcnob / (nreno + 1.0)), fenob));
  SEISCOMP_DEBUG(" ");
  SEISCOMP_DEBUG("%s %f %f %d %d %d %d %f %f %f %f", origin->publicID().c_str(),
                 fgap, frms, (int)narr, (int)nreto, (int)nreno, (int)nperc, qx,
                 pow((fgap / fcgap), fegap), pow((frms / fcrms), ferms),
                 pow((fcnob / (nreno + 1.0)), fenob));
  SEISCOMP_DEBUG("score: %f", score);

  return score;
}
