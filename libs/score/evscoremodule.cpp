/***************************************************************************
 *   Copyright (C) by ETHZ/SED                                             *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU Affero General Public License as published*
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Affero General Public License for more details.                     *
 *                                                                         *
 *                                                                         *
 * Developed by Stefan Heimers and Tobias Diehl, Swiss Seismological       *
 * Service Based on an example by Jan Becker of GEMPA                      *
 ***************************************************************************/

#include <Python.h>

#define SEISCOMP_COMPONENT EvScorePython

#include <seiscomp3/datamodel/origin.h>
#include <seiscomp3/logging/log.h>
#include "get_score.h"

using namespace std;

static PyObject *eventscore(PyObject *self, PyObject *args) {
  Seiscomp::DataModel::Origin *org;
  const char *org_publicid;
  double score;
  SEISCOMP_DEBUG("eventscore(): started...");
  if (!PyArg_ParseTuple(args, "s", &org_publicid)) {
    SEISCOMP_DEBUG("eventscore(): PyArg_ParseTuple failed");
    return NULL;
  }
  SEISCOMP_DEBUG("eventscore(): after PyArg_ParseTuple");
  SEISCOMP_DEBUG("eventscore(): org.publicid is: %s", org_publicid);

  // find the origin object for the public id
  org = Seiscomp::DataModel::Origin::Find(org_publicid);

  SEISCOMP_DEBUG("eventscore(): about to start get_score(org)");
  score = get_score(org);
  SEISCOMP_DEBUG("eventscore(): The score is: %f", score);
  return Py_BuildValue("d", score);
}

/*
 * Bind Python function names to our C functions
 */
static PyMethodDef myModule_methods[] = {
    {"eventscore", eventscore, METH_VARARGS}, {NULL, NULL}};


static struct PyModuleDef evscoremodule = {
    PyModuleDef_HEAD_INIT,
    "eventscore",   /* name of module */
    NULL, /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
    myModule_methods
};



/*
 * Python calls this to let us initialize our module
 */
PyMODINIT_FUNC PyInit_evscoremodule() {
   PyObject *m;
  m = PyModule_Create(&evscoremodule);
  if (m == NULL)
        return NULL;
  return m;     

}
