/***************************************************************************
 *   Copyright (C) by ETHZ/SED                                             *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU Affero General Public License as published*
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Affero General Public License for more details.                     *
 *                                                                         *
 *                                                                         *
 * Developed by Stefan Heimers and Tobias Diehl, Swiss Seismological       *
 * Service Based on an example by Jan Becker of GEMPA                      *
 ***************************************************************************/

/*!

    \brief Plugin to calculate a quality score based on residuals, azimuthal gap
   and standard error

    For testing:

    scevent --debug -E "smi:ch.ethz.sed/sc3rt/2013jjwy" -d \
        postgresql://<user>:<passwd>@rzseismo2.ethz.ch/<dbname>

    scevent.cfg example:
        plugins = ${plugins}, evscore
        eventAssociation.score = evscore
        eventAssociation.priorities =  SCORE, STATUS, AUTHOR, AGENCY, METHOD,
   RMS, PHASES
        evscore.default_score = 0
 */

#define SEISCOMP_COMPONENT EvScore

#include <seiscomp3/client/application.h>
#include <seiscomp3/config/config.h>
#include <seiscomp3/core/baseobject.h>
#include <seiscomp3/core/interfacefactory.h>
#include <seiscomp3/core/strings.h>
#include <seiscomp3/datamodel/event.h>
#include <seiscomp3/datamodel/origin.h>
#include <seiscomp3/logging/log.h>

#include "get_score.h"

using namespace std;

using namespace Seiscomp;
using namespace Seiscomp::DataModel;
using namespace Seiscomp::Client;

#include "evscore.h"

ADD_SC_PLUGIN("scevent score plugin (SED)",
              "Tobias Diehl / Stefan Heimers, SED", 0, 0, 1)

// constructor
EvScore::EvScore() { SEISCOMP_DEBUG("EvScore::EvScore() called"); }

/*!
\brief Read configuration file (currently not used)
*/
bool EvScore::setup(const Config::Config &config) {
  try {
    default_score = config.getDouble("evscore.default_score");
    SEISCOMP_DEBUG("default_score read from config: %f", default_score);
  } catch (...) {
    default_score = 0;
    SEISCOMP_DEBUG("default_score not configured, set to %f", default_score);
  }
  return true;
}

/*!
\brief This is called from scevent on each new or updated origin,
except when tests earlier in the priority list already fail.
*/
double EvScore::evaluate(Seiscomp::DataModel::Origin *origin) {
  double score;
  try {
    SEISCOMP_DEBUG("EvScore::evaluate called with Origin %s",
                   origin->publicID().c_str());
    score = get_score(origin);
    if (score == -1E23) {
      return default_score;
    } else {
      return score;
    }
  } catch (...) {
    SEISCOMP_ERROR("evaluate(): get_score() failed, skipping this origin");
    return default_score;
  }
}

REGISTER_ORIGINSCOREPROCESSOR(EvScore, "evscore");
