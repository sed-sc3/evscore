/***************************************************************************
 *   Copyright (C) by ETHZ/SED                                             *
 *                                                                         *
 * This program is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU Affero General Public License as published*
 * by the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                     *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU Affero General Public License for more details.                     *
 *                                                                         *
 *                                                                         *
 * Developed by Stefan Heimers and Tobias Diehl, Swiss Seismological       *
 * Service Based on an example by Jan Becker of GEMPA                      *
 ***************************************************************************/

#ifndef __EVSCORE_PLUGIN_H__
#define __EVSCORE_PLUGIN_H__

#include <seiscomp/core/plugin.h>
#include <seiscomp/plugins/events/scoreprocessor.h>

class EvScore : public Seiscomp::Client::ScoreProcessor {
 public:
  EvScore();

  bool setup(const Config::Config &config);

  double evaluate(Seiscomp::DataModel::Origin *origin);

  double evaluate(Seiscomp::DataModel::FocalMechanism *fm) { return 0; }

 private:
  double default_score; // value to be returned in case get_score() is unable to
                        // produce a correct score
};

#endif
